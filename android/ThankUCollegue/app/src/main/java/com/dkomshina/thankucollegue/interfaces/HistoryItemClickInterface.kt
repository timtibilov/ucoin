package com.dkomshina.thankucollegue.interfaces

import com.dkomshina.thankucollegue.model.HistoryItem

interface HistoryItemClickInterface {
    fun onHistoryItemClicked(position: Int, historyItem: HistoryItem)
}