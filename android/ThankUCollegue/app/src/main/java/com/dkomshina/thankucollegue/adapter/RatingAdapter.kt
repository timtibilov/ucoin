package com.dkomshina.thankucollegue.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.model.RatingItem
import kotlinx.android.synthetic.main.row_rating_item.view.*

class RatingAdapter(
    private val ratingList: ArrayList<RatingItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RowRatingItemHolder -> holder.bind(ratingList[position])
        }
    }

    override fun getItemCount() = ratingList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        RowRatingItemHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_rating_item,
                parent, false
            )
        )

    class RowRatingItemHolder(ratingItemView: View) : RecyclerView.ViewHolder(ratingItemView) {
        fun bind(ratingItem: RatingItem) {
            itemView.run {
                tv_rating_place.text = ratingItem.place.toString()
                tv_rating_name.text = ratingItem.name
                tv_rating_score.text = ratingItem.score.toString()
            }
        }
    }
}