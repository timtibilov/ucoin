package com.dkomshina.thankucollegue.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.model.BalanceItem
import com.dkomshina.thankucollegue.utils.Utils
import kotlinx.android.synthetic.main.fragment_balance.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Balance : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("Balance", "onCreateView")
        return inflater.inflate(R.layout.fragment_balance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadBalance()
        Log.d("Balance", "onViewCreated")
    }

    private fun loadBalance() {
        Log.d("Balance", "loading balance from Internet")
        Utils.apiInterface.getBalanceItem().enqueue(object : Callback<BalanceItem> {
            override fun onResponse(call: Call<BalanceItem>, response: Response<BalanceItem>) {
                Log.d("Balance", "balance onResponse")
                if (response.isSuccessful) {
                    runOnUI {
                        response.body()!!.run {
                            tv_available_for_transaction_amount.text = passiveBalance.toString()
                            tv_available_for_purchase_amount.text = activeBalance.toString()
                        }
                    }
                }
                Log.d("Balance", "balance loaded")
            }

            override fun onFailure(call: Call<BalanceItem>, t: Throwable) {
                Log.d("Balance", "onFailure")
            }
        })
    }

    private fun runOnUI(func: () -> Unit) {
        Handler(Looper.getMainLooper()).post(func)
    }

    companion object {
        fun newInstance(): Balance {
            val balance = Balance()
//            val bundle = Bundle()
//            bundle.putString("BOARD_NAME", board.name)
//            trelloBoardFragment.arguments = bundle
            return balance
        }
    }
}
