package com.dkomshina.thankucollegue.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dkomshina.thankucollegue.MainActivity
import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.interfaces.ItemStoreButtonClickInterface
import com.dkomshina.thankucollegue.model.StoreItem
import kotlinx.android.synthetic.main.fragment_store_item.*

class ItemStore(private val storeItem: StoreItem) : Fragment() {

    private val itemStoreButtonClickInterface: ItemStoreButtonClickInterface by lazy { context as MainActivity }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        Log.d("ItemStore", "onAttach")
        try {
            itemStoreButtonClickInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + " must implement ThankButtonClickInterface"
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("ItemStore", "onCreateView")
        return inflater.inflate(R.layout.fragment_store_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tv_store_item_description.text = storeItem.description
        tv_store_item_price.text = "${storeItem.price} UCoin"
        toolbar_store_item.title = storeItem.item_name
        cv_store_item.setOnClickListener {
            itemStoreButtonClickInterface.onItemStoreButtonClickListener();
        }
        Log.d("ItemStore", "onViewCreated")
    }
}