package com.dkomshina.thankucollegue.interfaces

import com.dkomshina.thankucollegue.model.StoreItem

interface StoreItemClickInterface {
    fun onStoreItemClicked(position: Int, storeItem: StoreItem)
}