package com.dkomshina.thankucollegue.interfaces

import com.dkomshina.thankucollegue.model.BalanceItem
import com.dkomshina.thankucollegue.model.HistoryItem
import com.dkomshina.thankucollegue.model.StoreItem
import com.dkomshina.thankucollegue.model.TransactionItem
import com.google.gson.annotations.SerializedName
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {
    @GET("/market/items")
    fun getMarketItems(): Call<List<StoreItem>>

    @GET("/transactions")
    fun getHistoryItems(): Call<List<HistoryItem>>

    @GET("/user/1")
    fun getBalanceItem(): Call<BalanceItem>

    @FormUrlEncoded
    @POST("/transactions/create")
    fun postTransactionItem(
        @Field("amount")
        amount: Int,
        @Field("sender_id")
        senderId: Long,
        @Field("receiver_id")
        receiverId: Long,
        @Field("description")
        description: String
    ): Call<TransactionItem>
}