package com.dkomshina.thankucollegue

import android.content.ContentValues
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Toast
import com.dkomshina.thankucollegue.fragment.*
import com.dkomshina.thankucollegue.interfaces.HistoryItemClickInterface
import com.dkomshina.thankucollegue.interfaces.ItemStoreButtonClickInterface
import com.dkomshina.thankucollegue.interfaces.StoreItemClickInterface
import com.dkomshina.thankucollegue.interfaces.ThankButtonClickInterface
import com.dkomshina.thankucollegue.model.HistoryItem
import com.dkomshina.thankucollegue.model.StoreItem
import com.dkomshina.thankucollegue.model.TransactionItem
import com.dkomshina.thankucollegue.utils.Utils
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity :
    AppCompatActivity(),
    HistoryItemClickInterface,
    StoreItemClickInterface,
    ItemStoreButtonClickInterface {

    private val onNavigationSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navigation_balance -> {
                    Log.d("Navigation","balance")
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, Balance.newInstance())
                        .commitAllowingStateLoss()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_history -> {
                    Log.d("Navigation","history")
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, History.newInstance())
                        .commitAllowingStateLoss()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_thank -> {
                    Log.d("Navigation","thank")
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, Thank.newInstance())
                        .commitAllowingStateLoss()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_store -> {
                    Log.d("Navigation","store")
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, Store.newInstance())
                        .commitAllowingStateLoss()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_rating -> {
                    Log.d("Navigation","rating")
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, Rating.newInstance())
                        .commitAllowingStateLoss()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationSelectedListener)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, Balance.newInstance())
                .addToBackStack(null)
                .commitAllowingStateLoss()
        }

        Log.d("MainActivity", "onCreated")
    }


//    override fun onThankButtonClickListener(
//        amount: Int,
//        senderId: Int,
//        receiverId: Int,
//        description: String
//    ) {
//        Log.d("Thank", "transaction creating")
//        Utils.apiInterface.postTransactionItem(amount, senderId, receiverId, description)
//            .enqueue(object : Callback<TransactionItem> {
//                override fun onResponse(
//                    call: Call<TransactionItem>,
//                    response: Response<TransactionItem>
//                ) {
//                    when {
//                        response.isSuccessful -> Toast.makeText(
//                            parent,
//                            "Перевод осуществлен",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                        response.code() == 422 -> Toast.makeText(
//                            parent,
//                            "Недостаточно средств",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                        else -> {
//                            Log.d("thank", "transaction created")
//                        }
//                    }
//                }
//
//                override fun onFailure(call: Call<TransactionItem>, t: Throwable) {
//                    Log.d("Thank", "onFailure")
//                }
//            })
//    }



    override fun onItemStoreButtonClickListener() {
        Log.d("ItemStore", "item bought")
        Toast.makeText(this, "Покупка", Toast.LENGTH_SHORT).show()
    }

    override fun onHistoryItemClicked(position: Int, historyItem: HistoryItem) {
        Toast.makeText(this, "$position item", Toast.LENGTH_SHORT).show()
    }

    override fun onStoreItemClicked(position: Int, storeItem: StoreItem) {
        Log.d("Store", "fragment item opening")
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ItemStore(storeItem))
            .addToBackStack(null)
            .commitAllowingStateLoss()
    }
}
