package com.dkomshina.thankucollegue.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.interfaces.StoreItemClickInterface
import com.dkomshina.thankucollegue.model.StoreItem
import kotlinx.android.synthetic.main.row_store_item.view.*

class StoreAdapter(
    private val storeList: ArrayList<StoreItem>,
    private val itemClickListener: StoreItemClickInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RowStoreItemHolder -> holder.bind(storeList[position])
        }
    }

    override fun getItemCount(): Int = storeList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        RowStoreItemHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_store_item,
                parent, false
            ), itemClickListener
        )

    class RowStoreItemHolder(
        storeItemView: View,
        private val itemClickListener: StoreItemClickInterface
    ) :
        RecyclerView.ViewHolder(storeItemView) {
        fun bind(storeItem: StoreItem) {
            itemView.run {
                tv_row_store_price.text = "${storeItem.price} UCoin"
                tv_row_store_name.text = "${storeItem.item_name}"

                layout_store_item.setOnClickListener {
                    itemClickListener.onStoreItemClicked(adapterPosition, storeItem)
                }
            }
        }
    }
}
