package com.dkomshina.thankucollegue.model

data class RatingItem(val name : String, val score : Int, val place: Int)