package com.dkomshina.thankucollegue.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.interfaces.HistoryItemClickInterface
import com.dkomshina.thankucollegue.model.HistoryItem
import kotlinx.android.synthetic.main.row_history_item.view.*

class HistoryAdapter(
    private val historyList: ArrayList<HistoryItem>,
    private val itemClickListener: HistoryItemClickInterface
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RowHistoryItemHolder -> holder.bind(historyList[position])
        }
    }

    override fun getItemCount() = historyList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        RowHistoryItemHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_history_item,
                parent, false
            ), itemClickListener
        )


    class RowHistoryItemHolder(
        historyItemView: View,
        private val itemClickListener: HistoryItemClickInterface
    ) : RecyclerView.ViewHolder(historyItemView) {

        fun bind(historyItem: HistoryItem) {
            when {
                historyItem.isPurchase -> {
                    itemView.tv_row_history_item_amount.text = "–${historyItem.amount}"
                    itemView.tv_row_history_item_name.text = historyItem.senderId.toString()
                    itemView.tv_row_history_item_where.text =
                        itemView.context.getString(R.string.from_purchase)
                    itemView.iv_row_history_item.setImageResource(R.drawable.ic_shopping_basket_black_24dp)
                }
                historyItem.senderId == 1L -> {
                    itemView.tv_row_history_item_amount.text = "–${historyItem.amount}"
                    itemView.tv_row_history_item_name.text = historyItem.receiverId.toString()
                    itemView.tv_row_history_item_where.text =
                        itemView.context.getString(R.string.from_transactions)
                    itemView.iv_row_history_item.setImageResource(R.drawable.ic_arrow_back_black_24dp)
                }
                historyItem.receiverId == 1L -> {
                    itemView.tv_row_history_item_amount.text = "+${historyItem.amount}"
                    itemView.tv_row_history_item_name.text = historyItem.senderId.toString()
                    itemView.tv_row_history_item_where.text =
                        itemView.context.getString(R.string.to_purchase)
                    itemView.tv_row_history_item_amount.setTextColor(itemView.context.getColor(R.color.brightTextColor))
                    itemView.iv_row_history_item.setImageResource(R.drawable.ic_arrow_forward_black_24dp)
                }
            }
            itemView.layout_row_history_item.setOnClickListener {
                itemClickListener.onHistoryItemClicked(adapterPosition, historyItem)
            }
        }
    }
}