package com.dkomshina.thankucollegue.fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.adapter.RatingAdapter
import com.dkomshina.thankucollegue.model.RatingItem
import kotlinx.android.synthetic.main.fragment_rating.*
import kotlinx.android.synthetic.main.fragment_store.*
import android.widget.ArrayAdapter




class Rating : Fragment() {
    private val ratingList = ArrayList<RatingItem>()
    private val ratingAdapter by lazy { RatingAdapter(ratingList) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("Rating", "onCreateView")
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        setAdapter()
        setSpinners()
        Log.d("Rating","onViewCreated")
    }

    private fun setSpinners() {
        val placeAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, arrayOf("Отдел", "Команда", "Офис"))
        placeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_rating_place.adapter = placeAdapter

        val dateAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, arrayOf("Неделя", "Месяц", "Год"))
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_rating_time.adapter = dateAdapter

        Log.d("Rating","spinners set")
    }

    private fun setAdapter() {
        rv_rating.adapter = ratingAdapter
        rv_rating.layoutManager = LinearLayoutManager(activity)

        Log.d("Rating","adapter set")
    }

    private fun setRecyclerView() {
        var max = 1000
        for (i in 1..20){
            ratingList.add(RatingItem("Item", max, i))
            max -= 10
        }
        Log.d("Rating","recycler view set")
    }

    companion object {
        fun newInstance(): Rating {
            val rating = Rating()
//            val bundle = Bundle()
//            bundle.putString("BOARD_NAME", board.name)
//            trelloBoardFragment.arguments = bundle
            return rating
        }
    }
}
