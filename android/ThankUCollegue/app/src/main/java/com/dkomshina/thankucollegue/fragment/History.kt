package com.dkomshina.thankucollegue.fragment


import android.content.ContentValues
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.dkomshina.thankucollegue.MainActivity

import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.adapter.HistoryAdapter
import com.dkomshina.thankucollegue.interfaces.HistoryItemClickInterface
import com.dkomshina.thankucollegue.model.*
import com.dkomshina.thankucollegue.utils.Utils
import kotlinx.android.synthetic.main.fragment_history.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class History : Fragment() {

    private val itemClickInterface: HistoryItemClickInterface by lazy { context as MainActivity }
    private val historyList = ArrayList<HistoryItem>()
    private val historyAdapter: HistoryAdapter by lazy {
        HistoryAdapter(historyList, itemClickInterface)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        Log.d("History", "onAttach")
        try {
            itemClickInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + " must implement HistoryItemClickInterface"
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("History", "onCreateView")
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        loadHistoryItems()
        Log.d("History", "onViewCreated")
    }

    private fun loadHistoryItems() {
        Utils.apiInterface.getHistoryItems().enqueue(object : Callback<List<HistoryItem>> {
            override fun onResponse(
                call: Call<List<HistoryItem>>,
                response: Response<List<HistoryItem>>
            ) {
                Log.d("History", "loading history items")
                if (response.isSuccessful) {
                    runOnUI {
                        historyList.clear()
                        response.body()?.forEach {
                            if (it.isPurchase || it.senderId == 1L || it.receiverId == 1L)
                                historyList.add(it)
                        }
                        historyList.reverse()
                        historyAdapter.notifyDataSetChanged()
                    }
                    Log.d("History", "history items loaded")
                }
            }

            override fun onFailure(call: Call<List<HistoryItem>>, t: Throwable) {
                Log.d("History", "onFailure")
            }
        })
    }

    private fun runOnUI(func: () -> Unit) {
        Handler(Looper.getMainLooper()).post(func)
    }

    private fun setRecyclerView() {
        rv_history.adapter = historyAdapter
        rv_history.layoutManager = LinearLayoutManager(activity)
        Log.d("History", "recycler view set")
    }

    companion object {
        fun newInstance(): History {
            val history = History()
//            val bundle = Bundle()
//            bundle.putString("BOARD_NAME", board.name)
//            trelloBoardFragment.arguments = bundle
            return history
        }
    }
}
