package com.dkomshina.thankucollegue.model

import com.google.gson.annotations.SerializedName

data class TransactionItem(
    val amount: Int,
    @SerializedName("sender_id")
    val senderId: Long,
    @SerializedName("receiver_id")
    val receiverId: Long,
    val description: String
)