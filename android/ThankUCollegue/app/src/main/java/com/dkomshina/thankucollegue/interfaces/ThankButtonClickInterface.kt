package com.dkomshina.thankucollegue.interfaces

import com.dkomshina.thankucollegue.model.TransactionItem

interface ThankButtonClickInterface {
    fun onThankButtonClickListener(
        amount: Int,
        senderId: Int,
        receiverId: Int,
        description: String
    )
}