package com.dkomshina.thankucollegue.model

import com.google.gson.annotations.SerializedName

data class HistoryItem(
    val amount: String,
    @SerializedName("sender_id")
    val senderId: Long,
    @SerializedName("receiver_id")
    val receiverId: Long,
    @SerializedName("time_stamp")
    val timeStamp: String,
    val description: String,
    @SerializedName("is_closed")
    val isClosed: Boolean,
    @SerializedName("is_hidden")
    val isHidden: Boolean,
    @SerializedName("is_purchase")
    val isPurchase: Boolean,
    @SerializedName("is_outgoing_transfer")
    val isOutgoingTransfer:Boolean
)