package com.dkomshina.thankucollegue.fragment


import android.content.ContentValues
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.dkomshina.thankucollegue.MainActivity

import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.interfaces.ThankButtonClickInterface
import com.dkomshina.thankucollegue.model.BalanceItem
import com.dkomshina.thankucollegue.model.TransactionItem
import com.dkomshina.thankucollegue.utils.Utils
import kotlinx.android.synthetic.main.fragment_balance.*
import kotlinx.android.synthetic.main.fragment_thank.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class Thank : Fragment() {

    //private val thankButtonClickInterface: ThankButtonClickInterface by lazy { context as MainActivity }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        Log.d("Thank", "onAttach")

        try {
            //thankButtonClickInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + " must implement ThankButtonClickInterface"
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("Thank", "onCreateView")
        return inflater.inflate(R.layout.fragment_thank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auto_tv_thank_place.setAdapter(
            ArrayAdapter<String>(
                context!!,
                android.R.layout.simple_dropdown_item_1line,
                resources.getStringArray(R.array.otdels)
            )
        )

        auto_tv_thank_name.setAdapter(
            ArrayAdapter<String>(
                context!!,
                android.R.layout.simple_dropdown_item_1line,
                resources.getStringArray(R.array.names)
            )
        )
        auto_tv_thank_name.threshold = 1
        auto_tv_thank_place.threshold = 1

        auto_tv_thank_name.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            if (b) {
                // Display the suggestion dropdown on focus
                auto_tv_thank_name.showDropDown()
            }
        }

        auto_tv_thank_place.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            if (b) {
                // Display the suggestion dropdown on focus
                auto_tv_thank_place.showDropDown()
            }
        }

        loadBalance()

        cv_thank_transaction.setOnClickListener {
            when {
                ed_thank_money.text.isEmpty() -> Toast.makeText(
                    context,
                    "Введите сумму перевода",
                    Toast.LENGTH_SHORT
                ).show()
                ed_thank_message.text.isEmpty() -> Toast.makeText(
                    context,
                    "Введите сообщение",
                    Toast.LENGTH_SHORT
                ).show()
                auto_tv_thank_name.text.isEmpty() -> Toast.makeText(
                    context,
                    "Введите ФИО",
                    Toast.LENGTH_SHORT
                ).show()
                auto_tv_thank_place.text.isEmpty() -> Toast.makeText(
                    context,
                    "Введите отдел",
                    Toast.LENGTH_SHORT
                ).show()
                else -> {
                    Utils.apiInterface.postTransactionItem(
                        ed_thank_money.text.toString().toInt(),
                        12L,
                        auto_tv_thank_name.text.toString().toLong(),
                        ed_thank_message.text.toString()
                    )
                        .enqueue(object : Callback<TransactionItem> {
                            override fun onResponse(
                                call: Call<TransactionItem>,
                                response: Response<TransactionItem>
                            ) {
                                when {
                                    response.isSuccessful -> Toast.makeText(
                                        context,
                                        "Перевод осуществлен",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    response.code() == 422 -> Toast.makeText(
                                        context,
                                        "Недостаточно средств",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    else -> {
                                        Toast.makeText(
                                            context,
                                            "Перевод сделан",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }

                            override fun onFailure(call: Call<TransactionItem>, t: Throwable) {
                                Toast.makeText(context, "NO", Toast.LENGTH_SHORT).show()
                            }
                        })
                    ed_thank_money.text.clear()
                    auto_tv_thank_name.text.clear()
                    ed_thank_message.text.clear()
                    auto_tv_thank_place.text.clear()
                }
            }
        }
        Log.d("Thank", "onViewCreated")
    }

    private fun loadBalance() {
        Log.d("Balance", "loading balance from Internet")
        Utils.apiInterface.getBalanceItem().enqueue(object : Callback<BalanceItem> {
            override fun onResponse(call: Call<BalanceItem>, response: Response<BalanceItem>) {
                Log.d("Balance", "balance onResponse")
                if (response.isSuccessful) {
                    runOnUI {
                        response.body()!!.run {
                            tv_thank_balance.text = passiveBalance.toString()
                        }
                    }
                }
                Log.d("Balance", "balance loaded")
            }

            override fun onFailure(call: Call<BalanceItem>, t: Throwable) {
                Log.d("Balance", "onFailure")
            }
        })
    }

    private fun runOnUI(func: () -> Unit) {
        Handler(Looper.getMainLooper()).post(func)
    }

    companion object {
        fun newInstance(): Thank {
            val thank = Thank()
//            val bundle = Bundle()
//            bundle.putString("BOARD_NAME", board.name)
//            trelloBoardFragment.arguments = bundle
            return thank
        }
    }
}
