package com.dkomshina.thankucollegue.model

import com.google.gson.annotations.SerializedName

data class StoreItem(
    @SerializedName("item_name")
    val item_name: String,
    val description: String,
    val image: String,
    val price: Int,
    val availability: Int,
    @SerializedName("is_on_sale")
    val is_on_sale: Boolean
)
