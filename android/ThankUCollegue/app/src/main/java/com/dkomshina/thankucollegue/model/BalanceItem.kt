package com.dkomshina.thankucollegue.model

import com.google.gson.annotations.SerializedName

data class BalanceItem(
    val id: Int,
    val email: String,
    val name: String,
    val surname: String,
    val photo: String,
    @SerializedName("is_active")
    val isActive: Boolean,
    @SerializedName("id_staff")
    val isStaff: Boolean,
    @SerializedName("active_balance")
    val activeBalance: Int,
    @SerializedName("passive_balance")
    val passiveBalance: Int
)