package com.dkomshina.thankucollegue.interfaces

import com.dkomshina.thankucollegue.model.StoreItem

interface ItemStoreButtonClickInterface {
    fun onItemStoreButtonClickListener()
}