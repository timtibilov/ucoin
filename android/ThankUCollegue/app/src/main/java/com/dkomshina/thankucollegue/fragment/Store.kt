package com.dkomshina.thankucollegue.fragment


import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.dkomshina.thankucollegue.MainActivity

import com.dkomshina.thankucollegue.R
import com.dkomshina.thankucollegue.adapter.StoreAdapter
import com.dkomshina.thankucollegue.interfaces.StoreItemClickInterface
import com.dkomshina.thankucollegue.model.StoreItem
import com.dkomshina.thankucollegue.utils.Utils
import kotlinx.android.synthetic.main.fragment_store.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Store : Fragment() {

    private val itemClickInterface: StoreItemClickInterface by lazy { context as MainActivity }
    private val storeList = ArrayList<StoreItem>()
    private val storeAdapter: StoreAdapter by lazy{
        StoreAdapter(storeList, itemClickInterface)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        Log.d("Store","onAttach")

        try {
            itemClickInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + " must implement StoreItemClickInterface"
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("Store","onCreateView")
        return inflater.inflate(R.layout.fragment_store, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        loadStoreItems()
        Log.d("Store","onViewCreated")
    }

    private fun setAdapter() {
        rv_store.adapter = storeAdapter
        rv_store.layoutManager = LinearLayoutManager(activity)
        Log.d("Store","adapter set")
    }

    private fun loadStoreItems() {
        Log.d("Store","load store items")
        Utils.apiInterface.getMarketItems().enqueue(object : Callback<List<StoreItem>> {
            override fun onResponse(call: Call<List<StoreItem>>, response: Response<List<StoreItem>>) {
                if (response.isSuccessful) {
                    runOnUI {
                        storeList.clear()
                        response.body()?.forEach {
                            storeList.add(it)
                        }
                        storeAdapter.notifyDataSetChanged()
                    }
                    Log.d("Store","store items loaded")
                }
            }

            override fun onFailure(call: Call<List<StoreItem>>, t: Throwable) {
                Log.d("Store", "onFailure")
            }
        })
    }

    private fun runOnUI(func: () -> Unit) {
        Handler(Looper.getMainLooper()).post(func)
    }

    companion object {
        fun newInstance(): Store {
            val store = Store()
//            val bundle = Bundle()
//            bundle.putString("BOARD_NAME", board.name)
//            trelloBoardFragment.arguments = bundle
            return store
        }
    }
}
