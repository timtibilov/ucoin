

async function /*Promise<json>*/ postJSONAsync(url, data) {
    // var data_body = JSON.stringify({
    //         email: "test@test.ru",
    //         password: "123456"
    //     });
    var data_body = JSON.stringify(data); // принимает json, возвращает string 
    console.log(data_body)
    let response = await fetch("https://cors-anywhere.herokuapp.com/" + url,{
        method: "POST",  // метаданные
        Origin: null, // метаданные
        headers: {   // заголовок
            'Content-Type': 'application/json'
        },
        body: data_body  // тело
    });
    // console.log('RESP '+response)
    // console.log('JSON '+await response.json())
    if (response.ok) {
        let json = await response.json();
        console.log("Result: ")
        console.log(json)
        return json;
    }
    else throw new Error(`${response.status}: ${response.statusText}`);
}


async function /*Promise<json>*/ postJSONAsync2(url, data) {
    // var data_body = JSON.stringify({
    //         email: "test@test.ru",
    //         password: "123456"
    //     });
    var data_body = JSON.stringify(data); // принимает json, возвращает string 
    console.log(data_body)
    let response = await fetch("https://cors-anywhere.herokuapp.com/" + url,{
        method: "POST",
        Origin: null,
        headers: {
            'Content-Type': 'application/json'
        },
        body: data_body
    });
    // console.log('RESP '+response)
    // console.log('JSON '+await response.json())
    if (response.ok) {
        alert("Перевод совершён успешно")
        return response;
    }
    else throw new Error(`${response.status}: ${response.statusText}`);
}


function logIn(email, code){
    // var data = {
    //         "email": "test@test.ru",
    //         "password": "123456"
    //     };
    var data = {
            "email": email,
            "password": code
        };
        // response это возвращаемый json (error если post запрос не успешен, обрабатывается дальше)
    postJSONAsync("http://renurtt.pythonanywhere.com/user/obtain_token/", data).then(response => {
        console.log("Post logIn response:")
        console.log(response)
        sessionStorage.setItem('person_name', response.name); // sessionStorage - локальное хранилище браузера (глоб переменная)
        sessionStorage.setItem('person_email', data.email);
        sessionStorage.setItem('person_token', response.token);
        // console.log(sessionStorage.getItem('person_name'));
        // console.log(sessionStorage.getItem('person_email'));
        // console.log(sessionStorage.getItem('person_token'));
        redirect("./balance.html");
    })
    .catch(() => {
        alert('Неверная почта или пароль!');
        console.log('ошибка');
    });
}


function register(name, surname, email, code){
    // var data = {
    //         "email": "test@test.ru",
    //         "password": "123456"
    //     };
    var data = {
            "name": name,
            "surname": surname,
            "email": email,
            "password": code
        };
    postJSONAsync("http://renurtt.pythonanywhere.com/user/create/", data).then(response => {
        console.log("Post register response:")
        console.log(response)
        // sessionStorage.setItem('person_name', name);
        // sessionStorage.setItem('person_surname', surname);
        // sessionStorage.setItem('person_email', email);
        // sessionStorage.setItem('person_token', code);
        // console.log(sessionStorage.getItem('person_name'));
        // console.log(sessionStorage.getItem('person_email'));
        // console.log(sessionStorage.getItem('person_token'));
        // redirect("./balance.html");
        alert('Вы успешно зарегистрированы, пожалуйста войдите в свой аккаунт!');
    })
    .catch(() => {
        alert('Неверно заполнены поля данный пользователь уже существует!');
        //console.log('ошибка');
    });
}

async function getAllUsers(){
    // console.log("Get all users")
    let response = await fetch("https://cors-anywhere.herokuapp.com/http://renurtt.pythonanywhere.com/user/users/",{
        method: "GET",
        Origin: null,
    });
    if (response.ok) {
        let json = await response.json();
        // console.log("Result: ")
        // console.log(json)
        return json;
    }
    else return null;

}

async function getIdBySurame(surname){
    let response = await fetch("https://cors-anywhere.herokuapp.com/http://renurtt.pythonanywhere.com/user/" + surname,{
        method: "GET",
        Origin: null,
    });
    if (response.ok) {
        let json = await response.json();
        // console.log("Result: ")
        // console.log(json)
        return json.id;
    }
    else return null;

}

async function getSurameById(id){
    let response = await fetch("https://cors-anywhere.herokuapp.com/http://renurtt.pythonanywhere.com/user/" + id,{
        method: "GET",
        Origin: null,
    });
    if (response.ok) {
        let json = await response.json();
        // console.log("Result: ")
        // console.log(json)
        return json.surname + ' ' + json.name;
    }
    else return null;

}

function makeTransaction(){
    document.getElementById("confirmation_dialogue").style.display = 'block'
}

function makeTransactionDenied(){
    document.getElementById("confirmation_dialogue").style.display = 'none'
}

async function makeTransactionConfirmed(){
    // var data = {
    //         "email": "test@test.ru",
    //         "password": "123456"
    //     };payment_fio_input
    
    document.getElementById("confirmation_dialogue").style.display = 'none'
    // var receiver_id = await getIdBySurame(document.getElementById("payment_fio_input").value)
    var receiver_id = document.getElementById("payment_fio_input").value.split(" ")[0]
    // console.log(receiver_id)
    if(receiver_id){
        var data = {
                "sender_id": sessionStorage.getItem('person_id'),
                "receiver_id": ''+receiver_id,
                "amount": document.getElementById("payment_amount_input").value,
                "description": document.getElementById("payment_comment_input").value
            };
        console.log(data)
        postJSONAsync2("http://renurtt.pythonanywhere.com/transactions/create/", data).then(response => {
            console.log("Post logIn response:")
            // if (response.ok) {
            console.log(response)
            // alert('Перевод совершён успешно!');
            // }
            // else
                // alert('Перевод не удался, проверьте количество средств на счету или повторите позднее!');
            
        }).catch(() => {
                alert('Перевод не удался, проверьте количество средств на счету или повторите позднее!');
                //console.log('ошибка');
            });
    }
    else
        alert('Неверная фамилия (имя) или с данной фамилией (именем) есть 2 и более человек!');
}


async function getByToken() { //  запрашиваем у сервера информацию о пользователе по токену (получаем при входе по логину и паролю)
  console.log('Get by token')
  var url = 'http://renurtt.pythonanywhere.com/user/update/'
  if(sessionStorage.getItem('person_token')){ // проверка существования токена
      var token = 'Bearer '+ sessionStorage.getItem('person_token')  
      let response = await fetch("https://cors-anywhere.herokuapp.com/" + url,{
            method: "GET",
            Origin: null,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        });
        if (response.ok) {
            let json = await response.json();
            console.log("Result of token: ")
            console.log(json)
            sessionStorage.setItem('person_name', json.name);
            sessionStorage.setItem('person_surname', json.surname);
            sessionStorage.setItem('person_email', json.email);
            document.getElementById("user_fio_cart").innerHTML = `
                                    <table>
                                        <tr><td>${json.name}</td></tr>
                                        <tr><td>${json.surname}</td></tr>
                                        <tr><td>${json.email}</td></tr>
                                    </table>
            `
            return json;
        }
        else {
            alert("Ваша сессия истекла, пожалуйста войдите снова");
            logOut();
        }
  }
  else{
    alert("Ваша сессия истекла, пожалуйста войдите снова");
    logOut();
  }
  // // An object of options to indicate where to post to
  // var options = {
  //     host: 'https://cors-anywhere.herokuapp.com/http://renurtt.pythonanywhere.com',
  //     port: '',
  //     path: '/user/update/',
  //     method: 'GET',
  //     headers: {
  //         'Content-Type': 'application/json',
  //         'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo3LCJ1c2VybmFtZSI6InRlc3RAdGVzdC5ydSIsImV4cCI6MTU4NDY1ODQ1MCwiZW1haWwiOiJ0ZXN0QHRlc3QucnUifQ.KPmVF0U5CQNYdcqaX0-QRT6EI2MWHm9NgzI73h2tsn8'
  //     }
  // };

  // // Set up the request
  // var req = http.request(options, function(res) {
  //     res.setEncoding('utf8');
  //     res.on('data', function (chunk) {
  //         console.log('Response: ' + chunk);
  //     });
  // });

  // // post the data
  // req.write();
  // req.end();

}


// app.use(clientSessions({
//   secret: '0GBlJZ9EKBt2Zbi2flRPvztczCewBxXK' // set this to a long random string!
// }));
// window.onload = function () {
//     var a = document.getElementById('head_table_of_payments_dropdown');
//     console.log(a);
//     a.onclick = function() {
//         document.getElementById('head_table_of_payments_dropdown_list').style.height = "100px";
//         console.log("clicked");
//         return false;
//     }
// }

var isHistoryListOpen = false;
var isHistoryListOpenListed = false;



async function setFIO(){
    return await getByToken();
}


document.onclick = function klicked() {
	console.log("doc",isHistoryListOpen);
	if(isHistoryListOpenListed && isHistoryListOpen){
		isHistoryListOpen = false;
		isHistoryListOpenListed = false;
		zeroItemHeight();
	}
	if(isHistoryListOpenListed && !isHistoryListOpen){
		isHistoryListOpen = true;
	}
}




let isSentKeyCode = false;
let isSentKeyCodeN = 0;

function changeVis(){
	// console.log(isSentKeyCode); 
	// if(!isSentKeyCode) {
	// 	document.getElementById("key_code").style.visibility = 'visible';
	// 	isSentKeyCode = true;
 //        isSentKeyCodeN = 0
	// }
	// else {
        var email_in = document.getElementById("email_input").value
        var key_code_in = document.getElementById("key_code_input").value
        document.getElementById("key_code_input").value = ''
        console.log(email_in)
		console.log(key_code_in)
		logIn(email_in, key_code_in);
        // getByToken();
        // if(isSentKeyCodeN > 0)
        //     redirect("./balance.html");
        // else
        //     isSentKeyCodeN += 1;
	// }
}


function registerUser(){
    var name_in = document.getElementById("name_register").value
    var surname_in = document.getElementById("surname_register").value
    var email_in = document.getElementById("email_register").value
    var key_code_in = document.getElementById("key_code_register").value
    document.getElementById("key_code_register").value = ''
    console.log(name_in)
    console.log(surname_in)
    console.log(email_in)
    console.log(key_code_in)
    register(name_in, surname_in, email_in, key_code_in);
}


// function logIn(){
//     console.log("Load market page")
//     // fetch("market/items",
//  //    {
//  //        method: "GET",
//  //        //body: data
//  //    })
//      postJSONAsync("http://renurtt.pythonanywhere.com/user/create/").then(response => {
//         console.log("LogIn response:")
//         console.log(response)
//         for(var i = 0; i < response.length; ++i){
//             // console.log(response[i])
//         }
        
//     })
//     .catch(() => console.log('ошибка'));
// }

// async function /*Promise<json>*/ postJSONAsync(url) {
//     let response = await fetch("https://cors-anywhere.herokuapp.com/" + url,{
//         method: "POST",
//         Origin: null,
//         body: {
//             "email": ["test"],
//             "name": ["testName"],
//             "surname": ["testSurname"],
//             "password": ["123456"]
//         }
//     });
//     return await response.json();
//     if (response.ok) {
//         let json = await response.json();
//         // console.log("Result: ")
//         // console.log(json)
//         return json;
//     }
//     else throw new Error(`${response.status}: ${response.statusText}`);
// }



function logOut(){
    redirect("./index.html");
}






function zeroItemHeight(){
	document.getElementById('head_table_of_payments_dropdown_list').style.height = "0px";
        return false;
}

function fullItemHeight(){
	console.log("item",isHistoryListOpen);
	if(!isHistoryListOpenListed){
		document.getElementById('head_table_of_payments_dropdown_list').style.height = "90px";
		isHistoryListOpenListed = true;
    }
        return false;
}




function showUserCart(){
	document.getElementById('user_fio_cart').style.height = "80px";
        return false;
}

function hideUserCart(){
	document.getElementById('user_fio_cart').style.height = "0px";
        return false;
}

var isHistroyTransactions = true

function setElementText(text){
    if(text === "История переводов")
        isHistroyTransactions = true
    else
        isHistroyTransactions = false
	document.getElementById('head_table_of_payments_dropdown_selected').innerHTML = text;

    fillHistoryTable();

    return false;
}



function redirect(url){
	if(typeof IE_fix != "undefined") // IE8 and lower fix to pass the http referer
    {
        document.write("redirecting...");
        var referLink = document.createElement("a");
        referLink.href = url;
        document.body.appendChild(referLink);
        referLink.click();
    }
    else { window.location.href = url; }
}



const historyTableHead = `
			<tr>
                <th style="width: 20%;">Номер отдела</th>
                <th style="width: 30%;">ФИО</th>
                <th style="width: 15%;">Цена</th>
                <th style="width: 15%;">Дата покупки</th>
                <th style="width: 20%;">Товар, количество</th>
            </tr>
`


const historyNewFromatTableHead = `
            <tr>
                <th style="width: 15%;">Номер отправителя</th>
                <th style="width: 15%;">Номер получателя</th>
                <th style="width: 15%;">Сумма</th>
                <th style="width: 20%; text-align: center;">Дата</th>
                <th style="width: 35%;">Комментарий</th>
            </tr>
`

const historyEmptyTableMessage = `
            <tr align="center" class="empty_data_message">
                <td colspan="5"> Нет данных.</td>
            </tr>
`

// const historyEmptyTableMessage = `
// 			<tr align="center" class="empty_data_message">
//                 <td colspan="5"> Нет данных, соответствующих условию. <br/>  Измените период или фильтры.</td>
//             </tr>
// `

const makegoodsTable = (arr) => {
	let str = "";
	for(var i = 0; i < arr.length; ++i){
		str += `
						<tr>
                            <td>${arr[i].type}</td>
                            <td>${arr[i].amount} шт.</td>
                        </tr>
		`
	}
	return str;
}

const makeHistoryRow = (data) => {return `
			<tr>
                <td>${data.num}</td>
                <td>${data.fio}</td>
                <td>${data.price}</td>
                <td>${data.date}</td>
                <td>
            		<table align="left">
                        ${makegoodsTable(data.array)}
                    </table>
                </td>
            </tr>
`}

const parceData = (data) => {
    let date = data.slice(0,10)
    let time = data.slice(11,19)
    return date + '\n' + time
}

const makeNewFormatHistoryRow = async (data) => {return `
            <tr>
                <td>${await getSurameById(data.sender_id)}</td>
                <td>${await getSurameById(data.receiver_id)}</td>
                <td>${data.amount}</td>
                <td>${parceData(data.time_stamp)}</td>
                <td>${data.description}</td>
            </tr>
`}

const makeHistoryRows = async (data) => {
	let str = "";
	for(var i = 0; i < data.length; ++i){
        // str += makeHistoryRow(data[i]);
		str += await makeNewFormatHistoryRow(data[i]);
	}
	return str;
}

const fillHistoryRows = async (data) => {
    let str = "";
    for(var i = data.length - 1; i >= 0 && isHistroyTransactions; --i){
        // str += makeHistoryRow(data[i]);
        var str2 = ""
        str2 += (await makeNewFormatHistoryRow(data[i]));
        str += str2;
        // console.log("Table " + i)
        // console.log(data[i]);
        if(!isHistroyTransactions)
            return "";
        document.getElementById('table_of_payments').innerHTML += str2;
        //document.getElementById('table_of_payments').innerHTML = document.getElementById('table_of_payments').innerHTML + (await makeNewFormatHistoryRow(data[i]));
    }
    console.log(str)
    return str;
}

function fillHistoryTable(){
    // console.log(isHistroyTransactions)
    if(!isHistroyTransactions){
        // console.log("true")
        document.getElementById('table_of_payments').innerHTML = historyTableHead
        document.getElementById('table_of_payments').innerHTML += historyEmptyTableMessage
    }
    else{
        // console.log("false")
    	//GET to api
    	// заглушка пока нет api и бд
    	// var data = [{num: 0, fio: "All", price: 42, date: "16.11.2018", array: [{type:"programmer", amount:1,},{type:"programmer2", amount:1,}]},
    	//             {num: 0, fio: "All", price: 42, date: "16.11.2018", array: [{type:"programmer", amount:1,},{type:"programmer2", amount:1,}]}];
    	// console.log(data);
    	// console.log(makeHistoryRows(data));
        var id = sessionStorage.getItem('person_id');
        var count = 0
        let strres = historyNewFromatTableHead;

        document.getElementById('table_of_payments').innerHTML = historyNewFromatTableHead;

        getJSONAsync("http://renurtt.pythonanywhere.com/transactions/sender_history/"+id).then(data => {
            console.log("Get response:")
            console.log(data)
            // for(var i = 0; i < data.length; ++i){
            //     console.log(data[i])
            // }

            if(data && data.length>0){
                count += data.length
                // document.getElementById('table_of_payments').innerHTML += makeHistoryRows(data);
                strres += fillHistoryRows(data);
            }
            if(!isHistroyTransactions) return "";
            getJSONAsync("http://renurtt.pythonanywhere.com/transactions/receiver_history/"+id).then(data => {
                console.log("Get response:")
                console.log(data)
                // for(var i = 0; i < data.length; ++i){
                //     console.log(data[i])
                // }

                if(data && data.length>0){
                    // document.getElementById('table_of_payments').innerHTML += makeHistoryRows(data);
                    count += data.length;
                    strres += fillHistoryRows(data);
                }

                if(!isHistroyTransactions) return "";
                
                if(count == 0)
                    document.getElementById('table_of_payments').innerHTML += historyEmptyTableMessage;
                // else
                    // document.getElementById('table_of_payments').innerHTML = strres
                
            })
            .catch(() => console.log('ошибка'));
                
            
        })
        .catch(() => console.log('ошибка'));

    }
}


const defaultGoodsList =`
<table class="fixed_layout">
            <tr>
                <td><p>
                    <img id="1" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
                <td><p>
                    <img id="2" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
                <td><p>
                    <img id="3" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
                <td><p>
                    <img id="4" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
            </tr>
            <tr>
                <td><p>
                    <img id="5" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
                <td><p>
                    <img id="6" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
                <td><p>
                    <img id="7" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
                <td><p>
                    <img id="8" alt="exit_logo" data-type="image" itemprop="image" src="images/market.jpg">
                    <br/>
                    Наименование
                    <br/>
                    Цена
                </p></td>
            </tr>
            
        </table>
`;


const createGood = (arr) => {
	let str = ""; /*src = "${arr.image}" */
	str += `
		<div>
            <div>${arr.description}</div>
            <img alt="${arr.item_name}" data-type="image" itemprop="image" src="images/market.jpg">  
            <br/>
            ${arr.item_name}
            <br/>
            Цена: ${arr.price}
            <br/>
            Количество: ${arr.availability}
        </div>
	`;
	return str;
}

const createGoodsList = (arr) => {
	let str = ""; /*src = "${arr.image}" */
	if(!arr || !arr.length){
		return defaultGoodsList;
	}
    // for(var i = 0, j = 0; j < arr.length; ++i){
	for(var j = 0; j < arr.length; ++j){
		str += `${createGood(arr[j])}`;
		// if(i === 1){
		// 	i = 0;
		// 	++j;
		// }
	}
	return str;
}



function getItemList(){
	console.log("Load market page")
	// fetch("market/items",
 //    {
 //        method: "GET",
 //        //body: data
 //    })
     getJSONAsync("http://renurtt.pythonanywhere.com/market/items").then(response => {
    	console.log("Get response:")
    	console.log(response)
    	// for(var i = 0; i < response.length; ++i){
    	// 	console.log(response[i])
    	// }
        document.getElementById('goods_tables').innerHTML = createGoodsList(response);
        
        
    })
    .catch(() => console.log('ошибка'));
}

async function /*Promise<json>*/ getJSONAsync(url) {
    let response = await fetch("https://cors-anywhere.herokuapp.com/" + url,{
    	method: "GET",
    	Origin: null,
    });
    if (response.ok) {
        let json = await response.json();
    	// console.log("Result: ")
        // console.log(json)
        return json;
    }
    else throw new Error(`${response.status}: ${response.statusText}`);
}

// document.getElementById("btn").onclick = btn_click;

// async function btn_click() {
//     let url = "https://engine.lifeis.porn/api/millionaire.php?q=4&count=1";
//     let json = await getJSONAsync(url);
//     document.getElementById("out").textContent = json.data.question;
// }

// var xmlhttp = new XMLHttpRequest();
// xmlhttp.onreadystatechange = function() {
//         if (this.readyState == 4 && this.status == 200) {
//             alert(this.responseText);
//         } else {
//         alert('Произошла ошибка!');
//     }
// };
 
// xmlhttp.open("GET", "./request.php", true);
// xmlhttp.send();



// var xmlhttp = new XMLHttpRequest();
// xmlhttp.onreadystatechange = function() {
//         if (this.readyState == 4 && this.status == 200) {
//             alert(this.responseText);
//         } else {
//         alert('Произошла ошибка!');
//     }
// };
 
// xmlhttp.open("GET", "https://cors-anywhere.herokuapp.com/http://renurtt.pythonanywhere.com/market/items", true);

// xmlhttp.send(null);

var showLogIn = true;
function logreg(){
    if(showLogIn){
        document.getElementById("logreg_type").value = "Вход";
        showLogIn = false;
        // document.getElementById("login_table_panel").style.height = '560px';
        // document.getElementById("login_table_panel").style.transition = "all 02.5s ease 2.5s";
        document.getElementById("login_table_panel").style.height = '0px';
        // document.getElementById("login_table_panel").style.transition = "all 0.0s";
        // document.getElementById("register_table_panel").style.height = '0px';
        // document.getElementById("register_table_panel").style.transition = "all 02.5s ease 2.5s";
        document.getElementById("register_table_panel").style.height = '450px';
        // document.getElementById("register_table_panel").style.transition = "all 0.0s";
        // document.getElementById("register_table_panel").style.height = '100%';
    }
    else{
        document.getElementById("logreg_type").value = "Регистрация";
        showLogIn = true;
        // document.getElementById("register_table_panel").style.height = '560px';
        document.getElementById("register_table_panel").style.height = '0px';
        document.getElementById("login_table_panel").style.height = '450px';
        // document.getElementById("login_table_panel").style.height = '100%';
    }
}



const makeUsersListItem = (user) => {
    return `<option>${user.id} ${user.surname} ${user.name} </option>`
}

async function fillUsersList(){
    var users = await getAllUsers();
    console.log(users)
    res = ""
    for(var i = 0; i < users.length; ++i)
        res += makeUsersListItem(users[i]) + '\n'
    document.getElementById("payment_fio_input_list").innerHTML = res
}







async function onloadBalance(){
    var json = await setFIO();
    // console.log(json)
    document.getElementById("active_balance_value").innerHTML = `
                        Активный Баланс
                        <br/>
                        <br/>
                        ${json.active_balance}
    `
    document.getElementById("passive_balance_value").innerHTML = `
                        Пассивный Баланс
                        <br/>
                        <br/>
                        ${json.passive_balance}
    `

    document.getElementById("balance_tables").style.visibility = 'visible';
}

async function onloadHistory(){
    var json = await setFIO();
    sessionStorage.setItem('person_id', json.id);
    fillHistoryTable();
}

async function onloadPayments(){
    var json = await setFIO();
    sessionStorage.setItem('person_id', json.id);
    fillUsersList();
    document.getElementById("payments_tables").style.opacity = "1.0";
    document.getElementById("payments_tables2").style.opacity = "1.0";
}

async function onloadMarket(){
    var json = await setFIO();
    getItemList();
}

async function onloadRating(){
    var json = await setFIO();
}